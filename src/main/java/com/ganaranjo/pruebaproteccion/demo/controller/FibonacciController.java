package com.ganaranjo.pruebaproteccion.demo.controller;

import com.ganaranjo.pruebaproteccion.demo.service.FibonacciService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
    Se requiere conocer los N primeros números de fibonacci
    Para las semillas X y Y
    Fibonacci es la sima de los 2 últimos números
    Las semillas son los minutos
    Los segundos son la cantidad
 */
@RestController
public class FibonacciController {

    FibonacciService fibonacciService = new FibonacciService();

    @Autowired
    JavaMailSender javaMailSender;
    String fibonacciText = "";

    public FibonacciController(){

    }

    @GetMapping("/fibonacci")
    public String GetFibonacciSerie(@RequestParam String mailto){

        // Generar la serie de fibinacci con base en la hora actual en el servidor
        fibonacciText = fibonacciService.GenerateFibonacciSerie();

        // enviar por correo
        sendMail(fibonacciText, mailto);
        return fibonacciText;
    }

    private void sendMail(String fibonacciText, String mailto){

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(mailto);
        message.setSubject("Prueba Técnica - Gustavo Naranjo Benavides");
        message.setText(fibonacciText);
        javaMailSender.send(message);
    }
}

package com.ganaranjo.pruebaproteccion.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class FibonacciService {

    public FibonacciService(){
    }

    public String GenerateFibonacciSerie(){

        int j = 2;
        int temp = 0;
        String fibonacciTexto = "";

        // Obtener la hora en string para manipular las semillas
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String hora = dateFormat.format(date);

        // Extrer cada semilla y la cantidad de la serie
        int semillaX = Integer.parseInt(hora.substring(3,4));
        int semillaY = Integer.parseInt(hora.substring(4,5));
        int cantidadSerie = Integer.parseInt(hora.substring(6,7)) + Integer.parseInt(hora.substring(7,8));
        int fibonacci[] = new int[cantidadSerie+2];

        // validar la predecendia de cada semilla
        if(semillaX>semillaY){
            temp = semillaY;
            semillaY = semillaX;
            semillaX = temp;
        }
        fibonacciTexto = semillaY + " " + semillaX;

        fibonacci[0] = semillaX;
        fibonacci[1] = semillaY;

        // iterar para obtener el resto de la serie según la cantidad
        for (int i = 0; i < cantidadSerie; i++)
        {
            fibonacci[j] = fibonacci[j-1] + fibonacci[j-2];
            fibonacciTexto = fibonacci[j] + " " + fibonacciTexto;
            j++;
        }

        fibonacciTexto = "Para la hora: " + hora + ", se produce la siguiente serie fibonacci " + fibonacciTexto;

        // entregar resultado al controller
        return fibonacciTexto;
    }
}
